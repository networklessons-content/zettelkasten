Title: VLAN Lab for CCNA

Tags: #ccna #vlan #lab #cisco 

Note:

This is a sample LAB topology:

![[lab-topology-sw1-sw2-h1-h2.png]]

## Goal

The goal of this lab is to:

- Create VLAN 10 and 20 manually on SW1 and SW2.
	- VLAN 10: name Computers
	- VLAN 20: name Printers
- Assign the interfaces connected to the hosts to VLAN 10.
- Configure a trunk between SW1 and SW2.

## Configuration

### VLANs

For a detailed explanation how to verify, create, or delete VLANs, check out:

[[202103251445 create vlan on cisco ios]]
[[202103251458 vlan delete on cisco ios]]

#### SW1

First, we configure a VLAN:

```
SW1(config)#vlan 10
SW1(config-vlan)#name Computers
SW1(config-vlan)#exit
```

```
SW1(config)#vlan 20
SW1(config-vlan)#name Printers
SW1(config-vlan)#exit
```

#### SW2

```
SW2(config)#vlan 10
SW2(config-vlan)#name Computers
SW2(config-vlan)#exit
```

```
SW2(config)#vlan 20
SW2(config-vlan)#name Printers
SW2(config-vlan)#exit
```

### Access Interfaces to hosts

For a detailed explanation how to assign interfaces to VLANs, check out:

[[202103251502 add interface to vlan in cisco ios]]

#### SW1

```
SW1(config)interface fa0/1
SW1(config-if)#switchport mode access
SW1(config-if)#switchport access vlan 10
```

#### SW2

```
SW1(config)interface fa0/1
SW1(config-if)#switchport mode access
SW1(config-if)#switchport access vlan 10
```

### Trunk between switches

For a detailed explanation of trunking configuration, check out:

[[202103251510 8021q trunk configuration on cisco ios]]

#### SW1

We configure the trunk encapsulation type and set the interface to trunking:

```
SW1(config)#interface fa0/14
SW1(config-if)#switchport trunk encapsulation dot1q
SW1(config-if)#switchport mode trunk
```

#### SW2

```
SW2(config)#interface fa0/14
SW2(config-if)#switchport trunk encapsulation dot1q
SW2(config-if)#switchport mode trunk
```

