Title: 802.1Q Trunk Configuration on Cisco IOS

Tags: #post #8021q #trunk #cisco #ios #configuration 

Note:

Before you create a trunk, make sure you create VLANs:

[[202103251445 create vlan on cisco ios]]

## Configuration

With the `switchport mode trunk` command you configure the interface as a trunk:

```
SW1(config)#interface fa0/14
SW1(config-if)#switchport mode trunk
Command rejected: An interface whose trunk encapsulation is "Auto" can not be configured to "trunk" mode.
```

The error you see above shows up because the trunk encapsulation type is set to auto. You need to choose between 802.1Q or ISL:

[[202103291352 trunk encapsulation types]]

With the `switchport trunk encapsulation` command you can change the trunk encapsulation type:

```
SW1(config)#interface fa0/14
SW1(config-if)#switchport trunk encapsulation ?
  dot1q      Interface uses only 802.1q trunking encapsulation when trunking
  isl        Interface uses only ISL trunking encapsulation when trunking
  negotiate  Device will negotiate trunking encapsulation with peer on interface
```

This is how to configure the trunk encapsulation type to 802.1Q:

```
SW1(config)#interface fa0/14
SW1(config-if)#switchport trunk encapsulation dot1q
```

Now you can change the interface to a trunk without an error:

```
SW1(config)#interface fa0/14
SW1(config-if)#switchport mode trunk
```

## Verification

Here's how to verify the trunk interface.

You can check the switchport information:

```
SW1#show interfaces fa0/14 switchport
Name: Fa0/14
Switchport: Enabled
Administrative Mode: dynamic auto 
Operational Mode: static access 
Administrative Trunking Encapsulation: dot1q
```

Or look at the trunk information:

```
SW2#show interface fa0/14 trunk 
Port        Mode             Encapsulation  Status        Native vlan
Fa0/14      on               802.1q         trunking      1
Port        Vlans allowed on trunk
Fa0/14      1-4094
Port        Vlans allowed and active in management domain
Fa0/14      1,50
Port        Vlans in spanning tree forwarding state and not pruned
Fa0/14      50
```

Links:

https://networklessons.com/cisco/ccna-200-301/how-to-configure-trunk-on-cisco-catalyst-switch