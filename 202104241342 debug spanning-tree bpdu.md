Title: Debug Spanning-Tree BPDU

Tags: #debug #stp #cisco 

Note:

To debug spanning-tree BPDUs on Cisco IOS, we can use the `SW2#debug spanning-tree bpdu` command.

When the switch sends or receives a BPDU, you see the following output:

```
SW2#
STP: VLAN0001 rx BPDU: config protocol = rstp, packet from FastEthernet0/14  , linktype IEEE_SPANNING , enctype 2, encsize 17 
STP: enc 01 80 C2 00 00 00 00 11 BB 0B 36 10 00 27 42 42 03 
STP: Data     000002023C10010011BB0B36000000000010010011BB0B360080100000140002000F00
STP: VLAN0001 Fa0/14:0000 02 02 3C 10010011BB0B3600 00000000 10010011BB0B3600 8010 0000 1400 0200 0F00
RSTP(1): Fa0/14 repeated msg
RSTP(1): Fa0/14 rcvd info remaining 6
RSTP(1): sending BPDU out Fa0/16
RSTP(1): sending BPDU out Fa0/17
STP: VLAN0001 rx BPDU: config protocol = rstp, packet f
```

Above, we see:

- A BPDU for VLAN1.
- Protocol (RSTP).
- The interface where we receive the BPDU on (FastEthernet0/14).
- The switch sends a BPDU on FastEthernet0/16 and 0/17.