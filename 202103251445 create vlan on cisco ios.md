Title: VLAN Configuration on Cisco IOS

Tags: #post #vlan #configuration #cisco #ios

Note:

We can see all VLANs on the switch using the `show vlan` command:

```
SW1#show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Fa0/1, Fa0/2, Fa0/3, Fa0/4
                                                Fa0/5, Fa0/6, Fa0/7, Fa0/8
                                                Fa0/9, Fa0/10, Fa0/12
                                                Fa0/13, Fa0/14, Fa0/22
                                                Fa0/23, Fa0/24, Gi0/1, Gi0/2
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup
```

By default, all interfaces are assigned to VLAN 1. You can also see the default VLANS: 1002, 1003, 1004 and 1005. You can find an explanation of these VLANs here:

[[202103251447 cisco ios default vlan 1002 - 1005]]

To create a VLAN, you can use the following command:

```
SW1(config)#vlan 50
SW1(config-vlan)#name Computers
SW1(config-vlan)#exit
```

The `name` command is optional. Here we see our new VLAN:

```
SW1#show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------

1    default                          active    Fa0/1, Fa0/2, Fa0/3, Fa0/4
                                                Fa0/5, Fa0/6, Fa0/7, Fa0/8
                                                Fa0/9, Fa0/10, Fa0/11, Fa0/12
                                                Fa0/13, Fa0/14, Fa0/15,
                                                Fa0/23, Fa0/24, Gi0/1, Gi0/2
50   Computers                        active
```

To delete a VLAN, there are two options:

[[202103251458 vlan delete on cisco ios]]

Links: