Title: VLAN is separate broadcast domain

Tags: #vlan #broadcast

Note:

Each VLAN is a broadcast domain. Broadcast traffic remains in the VLAN and is never forwarded from one VLAN to another.

![[building-multiple-switches.png]]

Links:

https://networklessons.com/cisco/ccna-200-301/introduction-to-vlans