Title: Trunk Encapsulation Types

Tags: #post #ccna #trunk #8021x #isl

Note:

There are two encapsulation types for trunks:

- [[202103251504 8021q encapsulation]]
- ISL

Links: